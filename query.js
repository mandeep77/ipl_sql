
const { Console } = require('console');
const csvtojson = require('csvtojson');
const path = require('path');
const { commit } = require('./index');
//const base = path.join(__dirname,);
const matches_ = './data/matches.csv';
const delivery_ = './data/deliveries.csv';
const connection = require('./index');


// function to insert data in matches table
function matches_main() {
    csvtojson()
        .fromFile(matches_) //fetching from matches.csv
        .then((matches) => {
            //console.log(matches);
            matches.forEach(match => {

                insertMatches(match);
            })

        }).catch(err => console.log(err))
}

// function to insert data in delivery table
function delivery_main() {
    csvtojson()
        .fromFile(delivery_) //fetching from matches.csv
        .then((delivery) => {
            //console.log(matches);


            insertDelivery(delivery);


        }).catch(err => console.log(err));
}
function insertMatches(matches) {


    let insert = 'INSERT INTO MATCHES SET ?'

    connection.query(insert, matches, function (err, result) {
        if (err) { console.log(err) }
        else { console.log(`inserted line in the table`); }
    });

}

function insertDelivery(objectArray) {

    let keys = Object.keys(objectArray[0]);
    let values = objectArray.map(obj => keys.map(key => obj[key]));
    let sql = 'INSERT INTO DELIVERIES' + ' (' + keys.join(',') + ') values ?';
    connection.query(sql, [values], function (err, result) {
        if (err) {
            console.log(err)
        }
        else { console.log(`inserted line in the table`); }
    });
}




//function to create database

function database(name) {
    let db_name = `CREATE DATABASE ${name}`;
    connection.query(db_name, function (err, result) {
        if (err) { console.log(err) }
        else {
            console.log(`database with name ${name} created`);
        }
    });



}

//function to create table named matches

function matches() {
    let query = `CREATE TABLE MATCHES(
        id  INT NOT NULL PRIMARY KEY,
        season varchar(100),
        city    varchar(100),
        date    date,
        team1   varchar(100),
        team2   varchar(100),
        toss_winner varchar(100),
        toss_decision varchar(100),
        result      varchar(50),
        dl_applied INT,
        winner varchar(100),
        win_by_runs INT,
        win_by_wickets INT,
        player_of_match varchar(100),
        venue varchar(100),
        umpire1 varchar(100),
        umpire2 varchar(100),
        umpire3 varchar(100)


    )`;

    connection.query(query, function (err, result) {
        if (err) {
            Console.log(err);
        } else {
            console.log(`created matches table`);
        }
    });


}

//function to create table named deliveries

function deliveries() {
    let query = `CREATE TABLE DELIVERIES (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        match_id INT,
        inning VARCHAR(100),
        batting_team VARCHAR(100),
        bowling_team VARCHAR(100),
        overs INT,
        ball INT,
        batsman VARCHAR(100),
        non_striker VARCHAR(100),
        bowler VARCHAR(100),
        is_super_over VARCHAR(50),
        wide_runs INT,
        bye_runs INT,
        legbye_runs INT,
        noball_runs INT,
        penalty_runs INT,
        batsman_runs INT,
        extra_runs INT,
        total_runs INT,
        player_dismissed VARCHAR(255),
        dismissal_kind VARCHAR(255),
        fielder VARCHAR(255),
        FOREIGN KEY (match_id) references MATCHES(id)
    )`

    connection.query(query, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            console.log(`created DELIVERY table`);
        }
    });
}





module.exports = { database, matches, matches_main, deliveries, delivery_main };